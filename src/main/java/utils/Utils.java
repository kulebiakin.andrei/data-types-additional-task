package utils;

public class Utils {
    public static void printDouble(double value, String comment) {
        System.out.printf("%s = %.2f\n", comment, value);
    }

    public static void printLong(long value, String comment) {
        System.out.printf("%s = %d\n", comment, value);
    }
}
