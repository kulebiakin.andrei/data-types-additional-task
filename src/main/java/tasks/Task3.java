package tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

import static utils.Utils.printDouble;

/**
 * Task 3. Calculate the value of the expression using the formula (all variables take real values):
 * (b + Math.sqrt(Math.pow(b, 2) + 4 * a * c)) / 2 * a - Math.pow(a, 3) * c + Math.pow(b, -2)
 */
public class Task3 {
    public static void main(String[] args) {
        double a;
        double b;
        double c;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter a:");
            a = scanner.nextDouble();
            System.out.println("Enter b:");
            b = scanner.nextDouble();
            System.out.println("Enter c:");
            c = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Numbers should be double");
            return;
        }

        double z = calculateFunction(a, b, c);
        printDouble(z, "z");
    }

    private static double calculateFunction(double a, double b, double c) {
        return (b + Math.sqrt(Math.pow(b, 2) + 4 * a * c)) / 2 * a - Math.pow(a, 3) * c + Math.pow(b, -2);
    }
}
