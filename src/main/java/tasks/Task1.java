package tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

import static utils.Utils.printDouble;

/**
 * Task 1. Given two real numbers x and y. Calculate their sum, difference, product and quotient.
 */
public class Task1 {
    public static void main(String[] args) {
        double x;
        double y;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter x:");
            x = scanner.nextDouble();
            System.out.println("Enter y:");
            y = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Numbers should be double");
            return;
        }

        double sum = calculateSum(x, y);
        printDouble(sum, "Sum");

        double difference = calculateDifference(x, y);
        printDouble(difference, "Difference");

        double product = calculateProduct(x, y);
        printDouble(product, "Product");

        double quotient = calculateQuotient(x, y);
        printDouble(quotient, "Quotient");
    }

    private static double calculateSum(double x, double y) {
        return x + y;
    }

    private static double calculateDifference(double x, double y) {
        return x - y;
    }

    private static double calculateProduct(double x, double y) {
        return x * y;
    }

    private static double calculateQuotient(double x, double y) {
        return x / y;
    }
}
