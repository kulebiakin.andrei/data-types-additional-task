package tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

import static utils.Utils.printDouble;

/**
 * Task 5. Calculate the distance between two points with the given coordinates (x1, y1) and (x2, y2).
 */
public class Task5 {
    public static void main(String[] args) {
        double x1;
        double y1;
        double x2;
        double y2;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter x1:");
            x1 = scanner.nextDouble();
            System.out.println("Enter y1:");
            y1 = scanner.nextDouble();
            System.out.println("Enter x2:");
            x2 = scanner.nextDouble();
            System.out.println("Enter y2:");
            y2 = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Numbers should be double");
            return;
        }

        double distance = calculateDistance(x1, y1, x2, y2);
        printDouble(distance, "Distance");
    }

    private static double calculateDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow((y2 - y1), 2) + Math.pow((x2 - x1), 2));
    }
}
