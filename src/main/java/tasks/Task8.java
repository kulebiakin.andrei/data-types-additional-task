package tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

import static utils.Utils.printLong;

/**
 * Task 8. Find the product of the digits of a given four-digit number.
 */
public class Task8 {
    public static void main(String[] args) {
        int number;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter integer number:");
            number = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Number should be Integer");
            return;
        }

        long product = calculateProductOfDigits(number);
        printLong(product, "Product");
    }

    private static long calculateProductOfDigits(int number) {
        if (number == 0) {
            return 0;
        }

        number = Math.abs(number);

        long product = 1;
        while (number > 0) {
            product = product * (number % 10);
            number = number / 10;
        }
        return product;
    }
}
