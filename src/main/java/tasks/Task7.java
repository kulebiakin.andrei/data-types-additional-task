package tasks;

/**
 * Task 7. Write a program, which displays the first four powers of a number π.
 */
public class Task7 {
    public static void main(String[] args) {
        for (int i = 0; i < 4; i++) {
            double pi = powPi(i);
            printPi(pi, i);
        }
    }

    private static double powPi(int power) {
        return Math.pow(Math.PI, power);
    }

    private static void printPi(double value, int power) {
        System.out.printf("PI ^ %d = %.6f\n", power, value);
    }
}
