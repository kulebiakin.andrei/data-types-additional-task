package tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

import static utils.Utils.printDouble;

/**
 * Task 6. Calculate the circumference and area of a circle of the same given radius R.
 */
public class Task6 {
    public static void main(String[] args) {
        double radius;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter radius: ");
            radius = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Radius should be double");
            return;
        }

        double length = calculateLength(radius);
        printDouble(length, "Length");

        double area = calculateArea(radius);
        printDouble(area, "Area");
    }

    private static double calculateLength(double radius) {
        return radius * 2 * Math.PI;
    }

    private static double calculateArea(double radius) {
        return Math.PI * Math.pow(radius, 2);
    }
}
