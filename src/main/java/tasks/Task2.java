package tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

import static utils.Utils.printDouble;

/**
 * Task 2. Find the value of the function: z = ((a - 3) * b / 2) + c
 */
public class Task2 {
    public static void main(String[] args) {
        double a;
        double b;
        double c;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter a:");
            a = scanner.nextDouble();
            System.out.println("Enter b:");
            b = scanner.nextDouble();
            System.out.println("Enter c:");
            c = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Numbers should be double");
            return;
        }

        double z = calculateFunction(a, b, c);
        printDouble(z, "z");
    }

    private static double calculateFunction(double a, double b, double c) {
        return ((a - 3) * b / 2) + c;
    }
}
