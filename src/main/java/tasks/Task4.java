package tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

import static utils.Utils.printDouble;

/**
 * Task 4. Calculate the perimeter and area of a right triangle by the lengths a and b of the two legs.
 */
public class Task4 {
    public static void main(String[] args) {
        double a;
        double b;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter a:");
            a = scanner.nextDouble();
            System.out.println("Enter b:");
            b = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Numbers should be double");
            return;
        }

        double perimeter = calculatePerimeter(a, b);
        printDouble(perimeter, "Perimeter");

        double area = calculateArea(a, b);
        printDouble(area, "Area");
    }

    private static double calculatePerimeter(double a, double b) {
        double hypotenuse = calculateHypotenuse(a, b);
        return a + b + hypotenuse;
    }

    private static double calculateHypotenuse(double a, double b) {
        return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    }

    private static double calculateArea(double a, double b) {
        return (a * b) / 2;
    }
}
