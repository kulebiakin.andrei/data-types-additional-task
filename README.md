1. Given two real numbers x and y. Calculate their sum, difference, product and quotient.
2. Find the value of the function: z = ((a - 3) * b / 2) + c
3. Calculate the value of the expression using the formula (all variables take real values):
(b + Math.sqrt(Math.pow(b, 2) + 4 * a * c)) / 2 * a - Math.pow(a, 3) * c + Math.pow(b, -2)
4. Calculate the perimeter and area of a right triangle by the lengths a and b of the two legs.
5. Calculate the distance between two points with the given coordinates (x1, y1) and (x2, y2).
6. Calculate the circumference and area of a circle of the same given radius R.
7. Write a program, which displays the first four powers of a number π.
8. Find the product of the digits of a given four-digit number.